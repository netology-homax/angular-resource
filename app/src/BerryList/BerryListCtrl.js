'use strict';

pokemonApp.controller('BerryListCtrl', function($scope, BerryService) {

    $scope.berries = BerryService.query();

});
